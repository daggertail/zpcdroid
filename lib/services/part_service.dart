import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zpcdroid/model/models.dart';

const kCollectionName = 'parts2';

class PartService {
  final _firestore = FirebaseFirestore.instance.collection(kCollectionName);

  Stream<List<Part>> getAllParts() {
    return _firestore.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return Part.fromJson({'id': doc.id, ...doc.data()});
      }).toList();
    });
  }

  Stream<List<Part>> getAllPartsByCategory(String category) {
    return _firestore
        .where('category', isEqualTo: category)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return Part.fromJson({'id': doc.id, ...doc.data()});
      }).toList();
    });
  }

  Future<List<Part>> getAllPartsByCategoryName(String categoryName) async {
    var doc = await _firestore.where('category', isEqualTo: categoryName).get();
    return doc.docs
        .map((doc) => Part.fromJson({'id': doc.id, ...doc.data()}))
        .toList();
  }

  Future<List<Part>> getPartsBasedOnArchAndCategory(arch, category) async {
    var doc = await _firestore
        .where('category', isEqualTo: category)
        .where('spec.arch', isEqualTo: arch)
        .get();

    return doc.docs
        .map((doc) => Part.fromJson({'id': doc.id, ...doc.data()}))
        .toList();
  }

  Future<List<dynamic>> getAllPartsWithIds(List<dynamic> partIds) async {
    var c =
        await _firestore.where(FieldPath.documentId, whereIn: partIds).get();

    return c.docs;
  }

  Future<List<Part>> getSnapshotOfPartsBasedOnCategory(
      String categoryName) async {
    var c = await _firestore.where('category', isEqualTo: categoryName).get();

    return c.docs
        .map((doc) => Part.fromJson({'id': doc.id, ...doc.data()}))
        .toList();
  }

  Future<void> updateDB() async {
    try {
      var querySnapshots = await _firestore.get();

      for (var doc in querySnapshots.docs) {
        await doc.reference.update({
          'competitorsPrice': [],
        });
      }
    } catch (e) {
      print(e);
    }
  }

  Future<List<Part>> getSelectablePartsByCategory(String category) async {
    var c = await _firestore.where('category', isEqualTo: category).get();

    return c.docs
        .map((doc) => Part.fromJson({'id': doc.id, ...doc.data()}))
        .toList();
  }

  Future<List<Part>> getCPUByArch(String arch) async {
    var c = await _firestore
        .where('category', isEqualTo: 'CPU')
        .where('spec.arch', isEqualTo: arch)
        .get();

    return c.docs
        .map((doc) => Part.fromJson({'id': doc.id, ...doc.data()}))
        .toList();
  }

  Future<List<Part>> getMoboBySocket(String socket) async {
    var c = await _firestore
        .where('category', isEqualTo: 'Motherboard')
        .where('spec.socket', isEqualTo: socket)
        .get();

    return c.docs
        .map((doc) => Part.fromJson({'id': doc.id, ...doc.data()}))
        .toList();
  }

  Future<List<Part>> getMemoryByType(String type) async {
    var c = await _firestore
        .where('category', isEqualTo: 'RAM')
        .where('spec.mem_type', isEqualTo: type)
        .get();

    return c.docs
        .map((doc) => Part.fromJson({'id': doc.id, ...doc.data()}))
        .toList();
  }
}
