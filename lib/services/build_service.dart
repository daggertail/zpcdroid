import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zpcdroid/controllers/controllers.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/services/part_service.dart';
import '../model/models.dart';

const kCollectionName = 'builds';
const kPartCollection = 'parts2';

class BuildService {
  final authController = Get.find<AuthController>();
  final _firestore = FirebaseFirestore.instance.collection(kCollectionName);

  Stream<List<Build>> getUserBuilds() {
    return _firestore.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        return Build.fromJson({'id': doc.id, ...doc.data()});
      }).toList();
    });
  }

  Future<List<Build>> getUserBuildWithId(String id) async {
    var doc = await _firestore.where('id', isEqualTo: id).get();
    return doc.docs
        .map((doc) => Build.fromJson({'id': doc.id, ...doc.data()}))
        .toList();
  }

  Future<void> deleteBuild(String id) async {
    await _firestore.doc(id).delete();
  }

  Future<void> saveTitle(String id, String newName) async {
    await _firestore.doc(id).update({'name': newName});
  }

  Future<void> saveBuild({
    required String name,
    required Map<String, Part> parts,
    required String price,
  }) async {
    final String id = _firestore.doc().id;
    final build = Build(
            id: id,
            uid: authController.userId!,
            name: name,
            price: price,
            parts: parts,
            status: 'New',
            created_on: DateTime.now(),
            updated_on: DateTime.now())
        .toJson();

    await _firestore.doc(id).set(build);
  }

  Future<void> updateStatus(
      {required String id, required String status}) async {
    await _firestore
        .doc(id)
        .update({'status': status, 'updated_on': DateTime.now()});
  }

  Future<void> updateBuild({
    required String id,
    required String name,
    required Map<String, Part> parts,
    required String price,
  }) async {
    await _firestore.doc(id).update({
      'name': name,
      'price': price,
      'parts': parts.map((k, e) => MapEntry(k, e.toJson())),
      'updated_on': DateTime.now()
    });
  }
}
