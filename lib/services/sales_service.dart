import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zpcdroid/controllers/controllers.dart';
import 'package:get/get.dart';
import '../model/models.dart';

const kCollectionName = 'sales';

class SalesService {
  final _firestore = FirebaseFirestore.instance.collection(kCollectionName);
  final authController = Get.find<AuthController>();

  Future<void> sendToStore(
      {required List<Part> items, String? title, String? customerName}) async {
    final String id = _firestore.doc().id;
    final sales = Sales(
            id: id,
            uid: authController.userId!,
            items: items,
            customerName: customerName ?? '',
            title: title ?? 'sales_${id.substring(id.length - 4)}',
            status: 'New',
            createdOn: DateTime.now(),
            updatedOn: DateTime.now())
        .toJson();

    await _firestore.doc(id).set(sales);
  }

  Future<void> sendPartsToStore(
      {required List<Part> items, String? customerName}) async {
    final String id = _firestore.doc().id;
    final sales = Sales(
            id: id,
            uid: authController.userId!,
            customerName: customerName ?? '',
            items: items,
            title: 'sales_${id.substring(id.length - 4)}',
            status: 'New',
            createdOn: DateTime.now(),
            updatedOn: DateTime.now())
        .toJson();

    await _firestore.doc(id).set(sales);
  }
}
