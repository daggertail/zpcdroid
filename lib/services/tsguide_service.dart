import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zpcdroid/model/models.dart';

const kCollectionName = 'tsguides';

class TSGuideService {
  final FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  Stream<List<TSGuide>> getAll() {
    return _firebaseFirestore
        .collection(kCollectionName)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return TSGuide.fromSnapshot(doc);
      }).toList();
    });
  }
}
