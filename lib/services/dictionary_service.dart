import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zpcdroid/model/models.dart';

const kCollectionName = 'dictionary';

class DictionaryService {
  final FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  Stream<List<Dictionary>> getAll() {
    return _firebaseFirestore
        .collection(kCollectionName)
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) {
        return Dictionary.fromSnapshot(doc);
      }).toList();
    });
  }

  Future<void> updateDB() async {
    try {
      var querySnapshots =
          await _firebaseFirestore.collection(kCollectionName).get();

      for (var doc in querySnapshots.docs) {
        await doc.reference.update({
          'citations': '',
        });
      }
    } catch (e) {
      print(e);
    }
  }
}
