import './constants.dart';

final formatCurrency = Constants.numberFormat;

String formatPrice(String price) {
  return formatCurrency.format(double.parse(price));
}
