import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/model/models.dart';
import 'package:zpcdroid/screens/build_detail_screen.dart';
import 'package:zpcdroid/screens/build_edit_screen.dart';
import '../screens/screens.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    // ignore: avoid_print
    print('This is route: $settings');

    switch (settings.name) {
      case '/':
        return WrapperScreen.route();

      case '/dashboard':
        return DashboardScreen.route();

      case '/login':
        return LoginScreen.route();

      case '/parts':
        return PartsScreen.route();

      case '/part-detail':
        return PartDetailScreen.route(
            settings.arguments as PartDetailScreenArgs);

      case '/user-builds':
        return UserBuildsScreen.route();

      case '/auto-build':
        return AutoBuildScreen.route();

      case '/build-cart':
        return WishlistScreen.route();

      case '/build-edit':
        return BuildEditScreen.route(b: settings.arguments as Build);

      case '/auto-build-detail':
        return AutoBuildDetailScreen.route();

      case '/choose-arch':
        return MBuildChooseArchScreen.route();

      case '/custom-build':
        return ManualBuildScreen.route();

      case '/tools':
        return ToolsScreen.route();

      case '/settings':
        return SettingsScreen.route();

      case '/dictionary':
        return DictionaryScreen.route();

      case '/troubleshooting':
        return TroubleshootingScreen.route();

      default:
        return _errorRoute();
    }
  }

  static Route _errorRoute() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: '/error'),
      builder: (BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('404: Cannot find route'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('We cannot find the screen.',
                  style: Theme.of(context).textTheme.headline2),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  Get.toNamed('/');
                },
                child: Text('Go back to home page'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
