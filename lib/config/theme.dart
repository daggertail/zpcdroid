import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData zpctheme(BuildContext context) {
  return ThemeData(
    textTheme: textTheme(context),
    colorScheme: ColorScheme.fromSwatch().copyWith(
      primary: const Color(0xFF003566),
      secondary: const Color(0xFF003566),
      error: const Color(0xFF003566),
      primaryVariant: const Color(0xFFFFC300),
      secondaryVariant: const Color(0xFFFFD60A),
    ),
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );
}

TextTheme textTheme(BuildContext context) {
  return GoogleFonts.notoSansTextTheme(Theme.of(context).textTheme).copyWith(
    headline1: const TextStyle(
      color: Colors.black,
      fontSize: 32,
      fontWeight: FontWeight.bold,
    ),
    headline2: const TextStyle(
      color: Colors.black,
      fontSize: 24,
      fontWeight: FontWeight.bold,
    ),
    headline3: const TextStyle(
      color: Colors.black,
      fontSize: 18,
      fontWeight: FontWeight.bold,
    ),
    headline4: const TextStyle(
      color: Colors.black,
      fontSize: 16,
      fontWeight: FontWeight.normal,
    ),
    headline5: const TextStyle(
      color: Colors.black,
      fontSize: 14,
      fontWeight: FontWeight.normal,
    ),
    headline6: const TextStyle(
      color: Colors.black,
      fontSize: 14,
      fontWeight: FontWeight.normal,
    ),
    bodyText1: const TextStyle(
      color: Colors.black,
      fontSize: 14,
      fontWeight: FontWeight.normal,
    ),
    bodyText2: const TextStyle(
      color: Colors.black,
      fontSize: 12,
      fontWeight: FontWeight.normal,
    ),
  );
}
