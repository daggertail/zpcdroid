import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/widgets/zpc_button.dart';
import '../controllers/controllers.dart';

const String kversion = '1.1.0';

class WelcomeScreen extends GetWidget<AuthController> {
  const WelcomeScreen({Key? key}) : super(key: key);

  static const String routeName = '/welcome';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (_) => const WelcomeScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          alignment: Alignment.bottomCenter,
          decoration: const BoxDecoration(
            color: Color(0xFF0b0244),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: 300.0,
                child: Image.asset('assets/images/welcome_graphic.png'),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const Text(
                      'Welcome to',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Text(
                      'ZPCDroid',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 38.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Text(
                      'Version $kversion',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12.0,
                      ),
                    ),
                    const SizedBox(
                      height: 30.0,
                    ),
                    Obx(
                      () => ZPCButton(
                        onPressed: () async {
                          await controller.signInAnonymously();
                        },
                        bgColor: const Color(0xFFffb703),
                        textColor: Colors.black,
                        title: 'Get Started',
                        isLoading: controller.isLoading.value,
                      ),
                    )
                  ],
                ),
              ),
              const Text(
                'Copyright ©2021  |  ZPCDroid version $kversion. All rights reserved.',
                style: TextStyle(color: Colors.white, fontSize: 10.0),
              )
            ],
          ),
        ),
      ),
    );
  }
}
