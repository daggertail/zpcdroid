import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/model/models.dart';
import 'package:zpcdroid/widgets/zpc_button.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import '../controllers/controllers.dart';

class BuildEditScreen extends GetWidget<BuildEditController> {
  static const String routeName = '/build-edit';

  final Build b;
  const BuildEditScreen({Key? key, required this.b}) : super(key: key);

  static Route route({required Build b}) {
    var c = Get.find<BuildEditController>();
    c.build.value = b.parts;
    c.title.value = b.name;

    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => BuildEditScreen(b: b),
    );
  }

  @override
  Widget build(BuildContext context) {
    final formatCurrency = Constants.numberFormat;
    final List<Category> categoryList = Category.categories;

    return Scaffold(
      appBar: ZPCAppBar(
        showLeading: true,
        title: b.name,
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 30.0),
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: SingleChildScrollView(
          child: Column(children: [
            ...categoryList
                .map((category) => SelectProductButton(
                      title: category.name,
                      category: category.type,
                      controller: controller,
                    ))
                .toList(),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
              child: TextButton(
                onPressed: () {
                  controller.showDeleteModal(b);
                },
                child: const Text(
                  'DELETE THIS BUILD',
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20)
          ]),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        mini: true,
        onPressed: () {
          Get.offAllNamed('/dashboard');
        },
        backgroundColor: Colors.grey[700],
        child: const Icon(Icons.home, size: 14),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.black,
        child: Container(
          width: double.infinity,
          margin: const EdgeInsets.symmetric(horizontal: 15),
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Obx(() => ZPCButton(
                    onPressed: controller.isBuildComplete.value
                        ? () {
                            controller.showUpdateTitleDialog(
                                isUpdate: true, id: b.id);
                          }
                        : null,
                    title: 'Update Build',
                  )),
              Obx(
                () => Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: 'TOTAL  ',
                        style: Theme.of(context)
                            .textTheme
                            .headline4!
                            .copyWith(color: Colors.white, fontSize: 14),
                      ),
                      TextSpan(
                        text:
                            'P ${formatCurrency.format(controller.totalPrice)}',
                        style: Theme.of(context)
                            .textTheme
                            .headline3!
                            .copyWith(color: Colors.white, fontSize: 14),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
