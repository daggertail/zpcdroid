import 'package:flutter/material.dart';
import 'package:zpcdroid/widgets/widgets.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  static const String routeName = '/settings';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (_) => SettingsScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ZPCAppBar(
          title: 'Settings',
        ),
        body: Container(
          margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
          child: ListView(children: [
            Row(
              children: [
                Text(
                  "Basic Settings",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color: Colors.grey[600]),
                )
              ],
            ),
            Divider(
              height: 15,
              thickness: 2,
            ),
            SizedBox(
              height: 10,
            ),
            //currency
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('sample'),
                        content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [
                              Text('Dollar'),
                              Text('Peso'),
                              Text('Canadian')
                            ]),
                        actions: [
                          FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('close')),
                        ],
                      );
                    });
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      "Currency",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.grey,
                    ),
                  ],
                ),
              ),
            ),
            //Language
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('sample'),
                        content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [
                              Text('Dollar'),
                              Text('Peso'),
                              Text('Canadian')
                            ]),
                        actions: [
                          FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('close')),
                        ],
                      );
                    });
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      "Language",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.grey,
                    ),
                  ],
                ),
              ),
            ),
            //display
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('sample'),
                        content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [
                              Text('Dollar'),
                              Text('Peso'),
                              Text('Canadian')
                            ]),
                        actions: [
                          FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('close')),
                        ],
                      );
                    });
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      "Display",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.grey,
                    ),
                  ],
                ),
              ),
            ),

            SizedBox(
              height: 30,
            ),

//APP

            Row(
              children: [
                Text(
                  "App",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color: Colors.grey[600]),
                )
              ],
            ),
            Divider(
              height: 15,
              thickness: 2,
            ),

            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Licenses'),
                        content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [Text('p'), Text('P'), Text('p')]),
                        actions: [
                          FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('close')),
                        ],
                      );
                    });
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Row(
                  children: const [
                    Icon(
                      Icons.card_membership_rounded,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      "Licenses",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),

            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('About'),
                        content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [Text('p'), Text('P'), Text('p')]),
                        actions: [
                          FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('close')),
                        ],
                      );
                    });
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Row(
                  children: const [
                    Icon(
                      Icons.info,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      "About",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Developer'),
                        content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [Text('p'), Text('P'), Text('p')]),
                        actions: [
                          FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('close')),
                        ],
                      );
                    });
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Row(
                  children: const [
                    Icon(
                      Icons.developer_board,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      "Developer",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Disclosure'),
                        content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [Text('p'), Text('P'), Text('p')]),
                        actions: [
                          FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('close')),
                        ],
                      );
                    });
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Row(
                  children: const [
                    Icon(
                      Icons.menu_book,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      "Disclosure",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
          ]),
        ));
  }
}
