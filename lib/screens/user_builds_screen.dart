import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/controllers/controllers.dart';
import 'package:zpcdroid/model/models.dart';
import 'package:zpcdroid/widgets/widgets.dart';

import '../constants.dart';

class UserBuildsScreen extends GetWidget<UserBuildsController> {
  const UserBuildsScreen({Key? key}) : super(key: key);

  static const String routeName = '/user-builds';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (_) => UserBuildsScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final formatCurrency = Constants.numberFormat;

    return Scaffold(
      appBar: const ZPCAppBar(
        showLogout: false,
        showLeading: true,
        title: 'User Builds',
      ),
      backgroundColor: const Color(0xFFFCFAF8),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Column(
          children: [
            Container(
              child:
                  Text('List of User Builds', style: Constants.headerDarkText),
            ),
            const SizedBox(height: 20),
            Expanded(
              child: Obx(
                () => controller.build.isNotEmpty
                    ? ListView.builder(
                        itemCount: controller.build.length,
                        itemBuilder: (ctx, idx) => Card(
                          child: ListTile(
                            title: Text(controller.build[idx].name,
                                style: Constants.regularDarkText),
                            enableFeedback: true,
                            onTap: () {
                              Get.toNamed('/build-edit',
                                  arguments: controller.build[idx]);
                            },
                            trailing: Icon(Icons.arrow_forward_ios),
                            // trailing: IconButton(
                            //   onPressed: () {
                            //     if (controller.build[idx].status != 'Sent') {
                            //       controller.showSendToStoreModal(
                            //           controller.build[idx]);
                            //     }
                            //   },
                            //   icon: controller.build[idx].status == 'Sent'
                            //       ? const Icon(Icons.check_box_outlined)
                            //       : const Icon(Icons.present_to_all_outlined),
                            //   color: controller.build[idx].status == 'Sent'
                            //       ? Colors.green
                            //       : Colors.grey[700],
                            // ),
                            subtitle: Text(
                                'Price: ${formatCurrency.format(double.parse(controller.build[idx].price))}  |  Date: ${Build.convertTimeStamp(controller.build[idx].updated_on)}'),
                          ),
                        ),
                      )
                    : Center(
                        child: Text('There\'s no build yet. Create now.'),
                      ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        mini: true,
        onPressed: () {
          Get.offAllNamed('/dashboard');
        },
        backgroundColor: Colors.grey[700],
        child: const Icon(Icons.home, size: 14),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.black,
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 15),
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ZPCButton(
                onPressed: () {
                  Get.toNamed('/choose-arch');
                },
                title: 'Manual Build',
                bgColor: Colors.blue.shade900,
              ),
              ZPCButton(
                onPressed: () {
                  Get.toNamed('/auto-build');
                },
                title: 'Auto Build',
                bgColor: const Color(0xFFffb703),
                textColor: Colors.black,
              )
            ],
          ),
        ),
      ),
    );
  }
}
