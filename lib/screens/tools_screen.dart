import 'package:flutter/material.dart';
import '../widgets/widgets.dart';
import 'package:get/get.dart';
import '../controllers/controllers.dart';

class ToolsScreen extends GetWidget<AuthController> {
  const ToolsScreen({Key? key}) : super(key: key);

  static const String routeName = '/tools';

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => const ToolsScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ZPCAppBar(
        showLeading: true,
        title: 'ZPCDroid Tools',
      ),
      backgroundColor: Color(0xFFFCFAF8),
      body: PageContainer(
        title: 'ZPCDroid Tools',
        description: 'Please select your options.',
        child: Container(
          height: MediaQuery.of(context).size.height - 240,
          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 30.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ZPCBoxButton(
                            title: 'Troubleshooting Guides',
                            image: 'assets/images/icon_screwdriver.png',
                            onPressed: () {
                              Get.toNamed('/troubleshooting');
                            }),
                        ZPCBoxButton(
                            title: 'Dictionary',
                            image: 'assets/images/icon_dictionary.png',
                            onPressed: () {
                              Get.toNamed('/dictionary');
                            }),
                      ],
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: const Text(
                    'ZPCDroid Copyright 2021',
                    textAlign: TextAlign.center,
                  ),
                )
              ]),
        ),
      ),
    );
  }

  Widget _buildCard({IconData? icon, String? title, VoidCallback? onPressed}) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: EdgeInsets.all(10.0),
        margin: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 3.0,
              blurRadius: 5.0,
            ),
          ],
          color: Colors.white,
        ),
        child: Column(
          children: [
            Icon(
              icon ?? Icons.mouse,
              size: 50.0,
            ),
            SizedBox(height: 10.0),
            Text(title ?? 'Text')
          ],
        ),
      ),
    );
  }
}
