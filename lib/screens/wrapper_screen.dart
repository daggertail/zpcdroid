import 'package:get/get.dart';
import 'package:flutter/material.dart';
import '../controllers/controllers.dart';
import './screens.dart';

class WrapperScreen extends GetWidget<AuthController> {
  const WrapperScreen({Key? key}) : super(key: key);

  static const String routeName = '/';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (_) => const WrapperScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Get.find<AuthController>().user != null
          ? const DashboardScreen()
          : const WelcomeScreen();
    });
  }
}
