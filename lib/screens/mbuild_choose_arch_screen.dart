import 'package:flutter/material.dart';
import 'package:zpcdroid/controllers/controllers.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/widgets/widgets.dart';

class MBuildChooseArchScreen extends GetWidget<ManualBuildController> {
  const MBuildChooseArchScreen({Key? key}) : super(key: key);

  static const String routeName = '/choose-arch';

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => const MBuildChooseArchScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ZPCAppBar(
        showLeading: true,
        title: 'Choose Architecture',
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 30.0),
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Text(
                'Choose Your Architecture',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 5.0),
              const Text(
                'Tap on box for for Intel or AMD type cpu',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 12.0,
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(
                    horizontal: 10.0, vertical: 50.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ZPCBoxButton(
                        title: 'Intel Based Computer',
                        image: 'assets/images/icon-intel.png',
                        onPressed: () {
                          controller.changeArch('intel');
                          Get.toNamed('/custom-build');
                        }),
                    ZPCBoxButton(
                        title: 'AMD Based Computer',
                        image: 'assets/images/icon-amd.png',
                        onPressed: () {
                          controller.changeArch('AMD');
                          Get.toNamed('/custom-build');
                        }),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
