import 'package:flutter/material.dart';
import 'package:zpcdroid/controllers/auth_controller.dart';
import '../widgets/widgets.dart';
import 'package:get/get.dart';

class DashboardScreen extends GetWidget<AuthController> {
  const DashboardScreen({Key? key}) : super(key: key);

  static const String routeName = '/dashboard';

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => const DashboardScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ZPCAppBar(
        showLeading: false,
      ),
      backgroundColor: Color(0xFFFCFAF8),
      body: PageContainer(
        title: 'Welcome to ZPCDroid',
        description: 'Please select your options.',
        child: Container(
          height: MediaQuery.of(context).size.height - 240,
          margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 30.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ZPCBoxButton(
                            title: 'View Parts',
                            image: 'assets/images/icon-parts.png',
                            onPressed: () {
                              Get.toNamed('/parts');
                            }),
                        ZPCBoxButton(
                            title: 'My Build',
                            image: 'assets/images/icon-build.png',
                            onPressed: () {
                              Get.toNamed('/user-builds');
                            }),
                      ],
                    ),
                    SizedBox(height: 30),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ZPCBoxButton(
                            title: 'Tools',
                            image: 'assets/images/icon-tool.png',
                            onPressed: () {
                              Get.toNamed('/tools');
                            }),
                        ZPCBoxButton(
                            title: 'Settings',
                            image: 'assets/images/icon-setting.png',
                            onPressed: () {
                              Get.toNamed('/settings');
                            }),
                      ],
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: const Text(
                    'ZPCDroid Copyright 2021',
                    textAlign: TextAlign.center,
                  ),
                )
              ]),
        ),
      ),
    );
  }

  // ListView(
  //         children: <Widget>[
  //           _buildCard(
  //               onPressed: () {
  //                 Get.toNamed('/parts');
  //               },
  //               title: 'PARTS'),
  //           _buildCard(
  //               onPressed: () {
  //                 Get.toNamed('/user-build');
  //               },
  //               title: 'BUILD'),
  //           _buildCard(
  //               onPressed: () {
  //                 Get.toNamed('/tools');
  //               },
  //               title: 'TOOLS'),
  //           _buildCard(
  //               onPressed: () {
  //                 Get.toNamed('/settings');
  //               },
  //               title: 'SETTINGS'),
  //           Container(
  //             margin: EdgeInsets.only(top: 10.0),
  //             child: const Text(
  //               'ZPCDroid Copyright 2021',
  //               textAlign: TextAlign.center,
  //             ),
  //           )
  //         ],
  //       ),

  // Container(
  //             margin: EdgeInsets.only(top: 10.0),
  //             child: const Text(
  //               'ZPCDroid Copyright 2021',
  //               textAlign: TextAlign.center,
  //             ),
  //           )

  Widget _buildCard({IconData? icon, String? title, VoidCallback? onPressed}) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: EdgeInsets.all(10.0),
        margin: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 3.0,
              blurRadius: 5.0,
            ),
          ],
          color: Colors.white,
        ),
        child: Column(
          children: [
            Icon(
              icon ?? Icons.mouse,
              size: 50.0,
            ),
            SizedBox(height: 10.0),
            Text(title ?? 'Text')
          ],
        ),
      ),
    );
  }
}
