import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/widgets/zpc_button.dart';
import '../widgets/widgets.dart';
import '../controllers/controllers.dart';
import '../constants.dart';

import 'package:flutter_svg/flutter_svg.dart';

class AutoBuildScreen extends GetWidget<AutoBuildController> {
  const AutoBuildScreen({Key? key}) : super(key: key);
  static const String routeName = '/auto-build';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (_) => AutoBuildScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final formatCurrency = Constants.numberFormat;
    controller.budgetTextController.text = '50000';

    return SafeArea(
      child: Scaffold(
        appBar: const ZPCAppBar(
          showLeading: true,
          title: 'Auto Build',
        ),
        body: Container(
          margin: const EdgeInsets.only(top: 30.0),
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          child: SingleChildScrollView(
              child: Column(
            children: [
              Text('Choose Your Preferred Architecture',
                  style: Theme.of(context)
                      .textTheme
                      .headline4!
                      .copyWith(fontWeight: FontWeight.bold)),
              SizedBox(height: 10),
              Obx(
                () => Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ZPCRadioButton(
                      onPressed: () {
                        controller.changeArch('intel');
                      },
                      title: 'Intel',
                      isSelected:
                          controller.arch.value == 'intel' ? true : false,
                    ),
                    const SizedBox(width: 20),
                    ZPCRadioButton(
                      onPressed: () {
                        controller.changeArch('AMD');
                      },
                      title: 'AMD',
                      isSelected: controller.arch.value == 'AMD' ? true : false,
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 30),
              Text('Enter Your Budget (Minimum of 50k)',
                  style: Theme.of(context)
                      .textTheme
                      .headline4!
                      .copyWith(fontWeight: FontWeight.bold)),
              const SizedBox(height: 10),
              TextFormField(
                maxLength: 6,
                decoration: const InputDecoration(
                  labelStyle: TextStyle(fontSize: 12),
                  labelText: 'Input Budget',
                  border: OutlineInputBorder(),
                ),
                keyboardType: TextInputType.number,
                inputFormatters: [
                  //only numeric keyboard.
                  LengthLimitingTextInputFormatter(6),
                ],
                controller: controller.budgetTextController,
                onChanged: (value) {
                  if (value.isNotEmpty) {
                    controller.setBudget(int.parse(value));
                  } else {
                    controller.setBudget(0);
                  }
                },
              ),
              const SizedBox(height: 10),
              Obx(
                () => Text(
                    'Your budget: P${formatCurrency.format(controller.budget.value)}',
                    style: Theme.of(context).textTheme.headline4),
              ),
              const SizedBox(height: 40),
              ZPCButton(
                  onPressed: () {
                    controller.autoBuildPC();
                  },
                  title: 'Build A PC')
            ],
          )),
        ),
      ),
    );
  }
}

class ZPCRadioButton extends StatelessWidget {
  final bool isSelected;
  final VoidCallback onPressed;
  final String title;
  const ZPCRadioButton({
    Key? key,
    this.isSelected = false,
    required this.onPressed,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      color: isSelected ? Color(0xFF003566) : Colors.grey,
      textColor: Colors.white,
      elevation: isSelected ? 3 : 0,
      child: Text(title.toUpperCase()),
    );
  }
}
