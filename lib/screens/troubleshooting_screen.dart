import 'dart:async';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zpcdroid/controllers/tsguide_controller.dart';
import 'package:zpcdroid/model/models.dart';
import '../widgets/widgets.dart';
import 'package:get/get.dart';

class TroubleshootingScreen extends GetWidget<TSGuideController> {
  const TroubleshootingScreen({Key? key}) : super(key: key);

  static const String routeName = '/troubleshooting';

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => const TroubleshootingScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    // FocusNode searchFieldFocusNode = FocusNode();
    // FocusScope.of(context).requestFocus(searchFieldFocusNode);

    Timer _debounce;

    final guideList = controller.searchedWord;

    return Scaffold(
      appBar: const ZPCAppBar(
        showLeading: true,
        title: 'Troubleshooting Guide',
      ),
      backgroundColor: const Color(0xFFFCFAF8),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(top: 15, bottom: 10),
              decoration: const BoxDecoration(color: Colors.teal),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 40,
                      alignment: Alignment.center,
                      margin: const EdgeInsets.only(left: 12.0, bottom: 8.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(24.0),
                      ),
                      child: TextFormField(
                        controller: controller.textController,
                        onChanged: (String text) {
                          _debounce =
                              Timer(const Duration(milliseconds: 1000), () {
                            controller.search();
                          });
                        },
                        decoration: const InputDecoration(
                          hintText: 'Search...',
                          isDense: true,
                          contentPadding: EdgeInsets.only(left: 24),
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                            fontSize: 16,
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: IconButton(
                      icon: const Icon(Icons.search, color: Colors.white),
                      onPressed: () {
                        controller.search();
                        // hide the onscreen keyboard
                        FocusScope.of(context).requestFocus(FocusNode());
                      },
                    ),
                  )
                ],
              ),
            ),
            Obx(
              () => ExpansionPanelList(
                children: guideList.asMap().entries.map((entry) {
                  int idx = entry.key;
                  var val = entry.value;

                  controller.isTitleOpen.add(false);

                  return ExpansionPanel(
                    headerBuilder: (context, isOpen) => Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        val.title,
                        style: Theme.of(context)
                            .textTheme
                            .headline5!
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                    ),
                    body: buildBody(context, val),
                    isExpanded: controller.isTitleOpen[idx],
                  );
                }).toList(),
                expansionCallback: (i, isOpen) {
                  controller.isTitleOpen[i] = !isOpen;
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildBody(BuildContext ctx, TSGuide guide) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      color: Colors.grey[200],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            guide.desc,
            style: Theme.of(ctx)
                .textTheme
                .headline6!
                .copyWith(fontWeight: FontWeight.bold),
          ),
          for (var i = 0; i < guide.steps.length; i++)
            Container(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('${i + 1}:',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  const SizedBox(width: 5),
                  Flexible(child: Text('${guide.steps[i]}'))
                ],
              ),
            ),
          if (guide.source.isNotEmpty)
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: TextButton(
                  onPressed: () {
                    _launchURL(guide.source);
                  },
                  child: Text('Source: ${guide.source}',
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                          fontSize: 11, fontStyle: FontStyle.italic)),
                ),
              ),
            )
        ],
      ),
    );
  }

  void _launchURL(_url) async {
    if (!await launch(_url)) throw 'Could not launch $_url';
  }
}
