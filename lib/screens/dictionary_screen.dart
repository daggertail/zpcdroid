import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/model/models.dart';
import '../widgets/widgets.dart';
import '../controllers/controllers.dart';

import 'package:url_launcher/url_launcher.dart';

class DictionaryScreen extends GetWidget<DictionaryController> {
  const DictionaryScreen({Key? key}) : super(key: key);

  static const String routeName = '/dictionary';

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => const DictionaryScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    Timer _debounce;

    return Scaffold(
      appBar: const ZPCAppBar(
        showLeading: true,
        title: 'ZPCDroid Dictionary',
      ),
      backgroundColor: const Color(0xFFFCFAF8),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(top: 15, bottom: 10),
              decoration: const BoxDecoration(color: Colors.blue),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 40,
                      alignment: Alignment.center,
                      margin: const EdgeInsets.only(left: 12.0, bottom: 8.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(24.0),
                      ),
                      child: TextFormField(
                        controller: controller.textController,
                        onChanged: (String text) {
                          _debounce =
                              Timer(const Duration(milliseconds: 1000), () {
                            controller.search();
                          });
                        },
                        decoration: const InputDecoration(
                          hintText: 'type in keywords...',
                          isDense: true,
                          contentPadding: EdgeInsets.only(left: 24),
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                            fontSize: 16,
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: IconButton(
                      icon: const Icon(Icons.search, color: Colors.white),
                      onPressed: () {
                        controller.search();
                        // hide the onscreen keyboard
                        FocusScope.of(context).requestFocus(FocusNode());
                      },
                    ),
                  )
                ],
              ),
            ),
            Obx(
              () => Container(
                height: MediaQuery.of(context).size.height - 170,
                margin: const EdgeInsets.all(15),
                child: ListView.builder(
                  itemCount: controller.searchedWord.length,
                  itemBuilder: (_, int) =>
                      dictBody(context, controller.searchedWord[int]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget dictBody(BuildContext ctx, Dictionary list) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      child: ListBody(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            color: Colors.grey[200],
            child: ListTile(
              title: Text(list.title, style: Theme.of(ctx).textTheme.headline4),
              dense: true,
              visualDensity: VisualDensity.compact,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18),
            child: Text(list.description),
          ),
          if (list.citations!.isNotEmpty)
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: TextButton(
                  onPressed: () {
                    _launchURL(list.citations);
                  },
                  child: Text('Source: ${list.citations}',
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                          fontSize: 11, fontStyle: FontStyle.italic)),
                ),
              ),
            )
        ],
      ),
    );
  }

  void _launchURL(_url) async {
    if (!await launch(_url)) throw 'Could not launch $_url';
  }
}
