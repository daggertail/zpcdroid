import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/screens/part_detail_screen.dart';
import '../controllers/controllers.dart';
import '../constants.dart';
import '../widgets/widgets.dart';

class WishlistScreen extends GetWidget<WishlistController> {
  static const String routeName = '/build-cart';
  const WishlistScreen({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => const WishlistScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final formatCurrency = Constants.numberFormat;

    return Scaffold(
      appBar: const ZPCAppBar(
        showLeading: true,
        title: 'My Wishlist',
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 30.0, left: 20, right: 20),
        child: Column(
          children: [
            const Text(
              'My Wishlist',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10),
            Expanded(
              child: Obx(
                () => controller.wishlist.isNotEmpty
                    ? ListView.builder(
                        itemCount: controller.wishlist.length,
                        itemBuilder: (context, idx) {
                          return Container(
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            child: ProductCard(
                              onDeleteButtonPressed: () {
                                controller.removeItem(controller.wishlist[idx]);
                              },
                              onPressed: () {
                                Get.toNamed('/part-detail',
                                    arguments: PartDetailScreenArgs(
                                      part: controller.wishlist[idx],
                                      showActionButton: false,
                                    ));
                              },
                              part: controller.wishlist[idx],
                            ),
                          );
                        })
                    : Center(
                        child: Text('Cart is empty.',
                            style: Theme.of(context).textTheme.headline4),
                      ),
              ),
            ),
          ],
        ),
      ),
      // floatingActionButton: Obx(() => FloatingActionButton(
      //       mini: true,
      //       onPressed: controller.wishlist.isNotEmpty
      //           ? () {
      //               controller.showSendToStoreModal();
      //             }
      //           : null,
      //       backgroundColor: controller.wishlist.isNotEmpty
      //           ? Colors.green
      //           : Colors.grey[700],
      //       child: const Icon(
      //         Icons.send,
      //         size: 14,
      //       ),
      //     )),
      // floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      bottomNavigationBar: BottomAppBar(
        color: Colors.black,
        child: Container(
          width: double.infinity,
          margin: const EdgeInsets.symmetric(horizontal: 35),
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Text(
                    'TOTAL',
                    style: Theme.of(context)
                        .textTheme
                        .headline4!
                        .copyWith(color: Colors.white),
                  ),
                  const SizedBox(width: 10),
                  Obx(
                    () => Text(
                      'P ${formatCurrency.format(controller.totalPrice)}',
                      style: Theme.of(context)
                          .textTheme
                          .headline3!
                          .copyWith(color: Colors.white),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
