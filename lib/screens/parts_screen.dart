import 'package:flutter/material.dart';
import 'package:zpcdroid/screens/screens.dart';
import '../controllers/controllers.dart';
import 'package:get/get.dart';
import '../widgets/widgets.dart';

class PartsScreen extends GetWidget<PartController> {
  const PartsScreen({Key? key}) : super(key: key);
  static const String routeName = '/parts';

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => const PartsScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    controller.getAllParts();

    return SafeArea(
      child: Scaffold(
        appBar: ZPCAppBar(
          showLeading: true,
          showSearch: true,
          searchCallback: controller.showSearchModal,
          title: 'View Parts',
        ),
        body: Column(
          children: [
            Obx(() {
              if (controller.searchKey.isEmpty) {
                return const SelectCategoryBar();
              } else {
                return searchResultBar();
              }
            }),
            Expanded(
              child: Obx(() => controller.parts.isNotEmpty
                  ? _buildList(
                      context,
                      controller.searchedParts.isNotEmpty
                          ? controller.searchedParts
                          : controller.parts)
                  : _buildEmptyList(context)),
            )
          ],
        ),
      ),
    );
  }

  Widget searchResultBar() {
    return Container(
      width: double.infinity,
      color: const Color(0xFF13293D),
      // decoration: BoxDecoration(color: Colors.white),
      padding: const EdgeInsets.all(10.0),
      child: SizedBox(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Result for: ${controller.searchKey}',
                style: const TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                  fontStyle: FontStyle.italic,
                ),
              ),
              IconButton(
                onPressed: () {
                  controller.clearSearch();
                },
                icon: const Icon(Icons.close_rounded),
                color: Colors.white,
              )
            ],
          )),
    );
  }

  Widget _buildList(context, parts) {
    final WishlistController cartController = Get.find<WishlistController>();

    return ListView.builder(
      shrinkWrap: true,
      itemCount: parts.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: Obx(
            () => !cartController.isPartAlreadyExistsInCart(parts[index])
                ? ProductCard(
                    part: parts[index],
                    onAddButtonPressed: () {
                      cartController.addItem(parts[index]);
                    },
                    onPressed: () {
                      Get.toNamed('/part-detail',
                          arguments: PartDetailScreenArgs(
                            part: parts[index],
                            showActionButton: true,
                          ));
                    },
                  )
                : ProductCard(
                    part: parts[index],
                    onDeleteButtonPressed: () {
                      cartController.removeItem(parts[index]);
                    },
                    onPressed: () {
                      Get.toNamed('/part-detail',
                          arguments: PartDetailScreenArgs(
                            part: parts[index],
                            showActionButton: true,
                          ));
                    },
                  ),
          ),
        );
      },
    );
  }

  Widget _buildEmptyList(context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
