import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/controllers.dart';
import '../widgets/widgets.dart';
import '../model/models.dart';

class PartDetailScreenArgs {
  final Part part;
  final bool showActionButton;

  PartDetailScreenArgs({
    required this.part,
    this.showActionButton = false,
  });
}

class PartDetailScreen extends GetWidget<WishlistController> {
  static const String routeName = '/part-detail';
  // final Part part;
  // final bool showActionButton;

  final PartDetailScreenArgs args;

  const PartDetailScreen({Key? key, required this.args}) : super(key: key);

  static Route route(PartDetailScreenArgs args) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => PartDetailScreen(args: args),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Part part = args.part;

    return SafeArea(
      child: Scaffold(
        appBar: ZPCAppBar(
          title: part.name,
        ),
        body: PartView(part: part),
        bottomNavigationBar: _buildBottomAppBar(context),
      ),
    );
  }

  Widget _buildBottomAppBar(BuildContext context) {
    final bool showActionButton = args.showActionButton;
    final Part part = args.part;

    return BottomAppBar(
      // color: Colors.black,
      child: SizedBox(
        height: 70,
        child: Row(
          mainAxisAlignment: showActionButton
              ? MainAxisAlignment.spaceAround
              : MainAxisAlignment.center,
          children: [
            TextButton(
              child: const Text('Back'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            showActionButton
                ? Obx(
                    () => !controller.isPartAlreadyExistsInCart(part)
                        ? ZPCButton(
                            title: 'Add To Cart',
                            onPressed: () {
                              controller.addItem(part);
                            },
                          )
                        : ZPCButton(
                            title: 'Remove from Cart',
                            bgColor: Colors.red,
                            onPressed: () {
                              controller.removeItem(part);
                            },
                          ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
