import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/screens/part_detail_screen.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import '../model/models.dart';
import '../controllers/controllers.dart';

class BuildDetailScreen extends GetWidget<UserBuildsController> {
  static const String routeName = '/build-detail';
  final Build b;
  const BuildDetailScreen({Key? key, required this.b}) : super(key: key);

  static Route route({required Build b}) {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => BuildDetailScreen(b: b),
    );
  }

  @override
  Widget build(BuildContext context) {
    // controller.loadBuildParts(b.part_ids);

    final formatCurrency = Constants.numberFormat;
    controller.setTitle(b.name);

    final List<Category> categoryList = Category.categories;

    return Scaffold(
      appBar: const ZPCAppBar(
        showLeading: true,
        title: 'Build Details',
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 30.0, left: 20, right: 20),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Obx(
                      () => Text(
                        controller.buildTitle.value,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    GestureDetector(
                      onTap: () {
                        controller.showUpdateTitleDialog(b.id, b.name);
                      },
                      child: const Icon(
                        Icons.edit,
                        color: Colors.black45,
                      ),
                    )
                  ],
                ),
                const Divider(),
                const SizedBox(height: 5.0),
                ...categoryList.map((category) {
                  final part = b.parts[category.type];

                  return Container(
                    margin: const EdgeInsets.only(top: 20, bottom: 20),
                    child: Column(
                      children: [
                        Text(
                          category.name,
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.headline2,
                        ),
                        const SizedBox(height: 10),
                        if (part != null)
                          ProductCard(
                            onPressed: () {
                              Get.toNamed('/part-detail',
                                  arguments: PartDetailScreenArgs(
                                    part: part,
                                    showActionButton: false,
                                  ));
                            },
                            part: part,
                          )
                      ],
                    ),
                  );
                }).toList()
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.black,
        child: Container(
          width: double.infinity,
          margin: const EdgeInsets.symmetric(horizontal: 35),
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                onPressed: () {
                  controller.deleteBuild(b.id);
                },
                child: const Text(
                  'DELETE',
                  style: TextStyle(color: Colors.red),
                ),
              ),
              Row(
                children: [
                  Text(
                    'TOTAL',
                    style: Theme.of(context)
                        .textTheme
                        .headline4!
                        .copyWith(color: Colors.white),
                  ),
                  const SizedBox(width: 10),
                  Text(
                    'P ${formatCurrency.format(double.parse(b.price))}',
                    style: Theme.of(context)
                        .textTheme
                        .headline3!
                        .copyWith(color: Colors.white),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
