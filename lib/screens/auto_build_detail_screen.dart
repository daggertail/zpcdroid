import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/model/models.dart';
import 'package:zpcdroid/widgets/zpc_button.dart';
import '../constants.dart';
import '../widgets/widgets.dart';
import '../controllers/controllers.dart';

class AutoBuildDetailScreen extends GetWidget<AutoBuildController> {
  static const String routeName = '/auto-build-detail';
  const AutoBuildDetailScreen({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute(
      settings: const RouteSettings(name: routeName),
      builder: (_) => const AutoBuildDetailScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final formatCurrency = Constants.numberFormat;
    final List<Category> categoryList = Category.categories;

    return Scaffold(
      appBar: const ZPCAppBar(
        showLeading: true,
        title: 'Autobuild Result',
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 30.0),
        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
        child: SingleChildScrollView(
            child: Column(
          children: categoryList
              .map((category) => SelectProductButton(
                    title: category.name,
                    category: category.type,
                    controller: controller,
                  ))
              .toList(),
        )),
      ),
      floatingActionButton: FloatingActionButton(
        mini: true,
        onPressed: () {
          Get.offAllNamed('/dashboard');
        },
        backgroundColor: Colors.grey[700],
        child: const Icon(Icons.home, size: 14),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.black,
        child: Container(
          width: double.infinity,
          margin: const EdgeInsets.symmetric(horizontal: 15),
          height: 70,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Obx(() => ZPCButton(
                    onPressed: controller.isBuildComplete.value
                        ? controller.showUpdateTitleDialog
                        : null,
                    title: 'Save this Build',
                  )),
              Obx(
                () => Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: 'TOTAL  ',
                        style: Theme.of(context)
                            .textTheme
                            .headline4!
                            .copyWith(color: Colors.white, fontSize: 14),
                      ),
                      TextSpan(
                        text:
                            'P ${formatCurrency.format(controller.totalPrice)}',
                        style: Theme.of(context)
                            .textTheme
                            .headline3!
                            .copyWith(color: Colors.white, fontSize: 14),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
