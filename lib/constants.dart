import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Constants {
  static const regularHeading = TextStyle(
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
    color: Color(0xFF3A0CA3),
  );

  static const boldHeading = TextStyle(
    fontSize: 22.0,
    fontWeight: FontWeight.w600,
    color: Color(0xFF3A0CA3),
  );

  static const regularDarkText = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
    color: kMainTextColor,
  );

  static const headerDarkText = TextStyle(
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
    color: kMainTextColor,
  );

  static final numberFormat = NumberFormat("###,###", "en_US");
}

const kMainColor = Color(0xFF0b0244);
const kMainTextColor = Colors.black;

const kDefaultMargin = 40;
const kDefaultPadding = 20;
