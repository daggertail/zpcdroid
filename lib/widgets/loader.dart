import 'package:flutter/material.dart';
import 'widgets.dart';

class Loader extends StatelessWidget {
  const Loader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height:
          MediaQuery.of(context).size.height / 2 - 60, // minus appbar height
      padding: const EdgeInsets.symmetric(horizontal: 100),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: const [
          LinearProgressIndicator(
            minHeight: 10,
          ),
          SizedBox(height: 20),
          Text('Loading...')
        ],
      ),
    );
  }
}
