import 'package:flutter/material.dart';
import 'package:get/get.dart';

void showSnackbar({
  required String title,
  required String message,
  bool? isError,
  bool? isSuccess,
  bool? isWarning,
}) {
  Color backgroundColor = Colors.blue;
  Color colorText = Colors.black;

  if (isSuccess != null && isSuccess) {
    backgroundColor = Colors.greenAccent.shade700;
  } else if (isError != null && isError) {
    backgroundColor = Colors.red.shade900;
    colorText = Colors.white;
  } else if (isWarning != null && isWarning) {
    backgroundColor = Colors.amber.shade900;
  }

  return Get.snackbar(
    title,
    message,
    snackPosition: SnackPosition.TOP,
    backgroundColor: backgroundColor,
    colorText: colorText,
    duration: Duration(seconds: 2),
    margin: EdgeInsets.all(20),
    dismissDirection: SnackDismissDirection.HORIZONTAL,
  );
}
