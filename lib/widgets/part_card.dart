import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/controllers.dart';
import '../model/models.dart';

class PartCard extends StatelessWidget {
  final Part part;
  final bool? showAddButton;
  final bool? showDeleteButton;
  final bool? showTrailingButtons;

  const PartCard({
    Key? key,
    required this.part,
    this.showAddButton = true,
    this.showDeleteButton = false,
    this.showTrailingButtons = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ManualBuildController controller = Get.put(ManualBuildController());

    return InkWell(
      onTap: () {
        // Get.toNamed('/part-detail', arguments: part);
        Navigator.pushNamed(context, '/part-detail', arguments: part);
      },
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
        padding: const EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 3.0,
              blurRadius: 5.0,
            ),
          ],
          color: Colors.white,
        ),
        child: Row(
          children: [
            part.images.isNotEmpty
                ? Image.network(part.images[0], width: 100, fit: BoxFit.cover)
                : Image.asset('assets/images/placeholder.jpg', width: 100.0),
            Flexible(
              child: Container(
                margin: EdgeInsets.only(left: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      part.name,
                      style: Theme.of(context).textTheme.headline6,
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        Column(
                          children: buildSpecsWidget(
                                  ['socket', 'type', 'mem_type'], part)
                              .toList(),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        Text(
                          'P${part.price}',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 10.0, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            showTrailingButtons == true
                ? (showAddButton == true && showDeleteButton == false)
                    ? IconButton(
                        icon: Icon(Icons.add_circle),
                        iconSize: 32.0,
                        color: Color(0xFF028090),
                        onPressed: () {
                          Navigator.pop(context);
                          controller.addPart(part);
                        },
                      )
                    : IconButton(
                        icon: Icon(Icons.remove_circle_outline_sharp),
                        iconSize: 22.0,
                        color: Colors.red,
                        onPressed: () {
                          controller.removePart(part);
                        },
                      )
                : Container()
          ],
        ),
      ),
    );
  }

  List<Widget> buildSpecsWidget(List<String> keys, Part part) {
    List<Widget> widgets = [];
    for (var k in keys) {
      if (part.spec!.containsKey(k)) {
        widgets.add(Text(
          "${k.toUpperCase()}: ${part.spec![k]}",
          textAlign: TextAlign.left,
          style: TextStyle(
            fontSize: 10.0,
          ),
        ));
      }
    }
    return widgets;
  }
}
