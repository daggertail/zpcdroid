import 'package:flutter/material.dart';

class ZPCBoxButton extends StatelessWidget {
  final String image;
  final String title;
  final VoidCallback onPressed;

  const ZPCBoxButton({
    Key? key,
    required this.title,
    required this.image,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        width: MediaQuery.of(context).size.width / 2 - 50,
        height: 130,
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black12.withOpacity(.05),
                spreadRadius: 3.0,
                blurRadius: 4.0)
          ],
          borderRadius: BorderRadius.circular(20),
        ),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              image,
              width: 50,
            ),
            SizedBox(height: 20),
            Flexible(
              child: Text(title,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline5),
            ),
          ],
        ),
      ),
    );
  }
}
