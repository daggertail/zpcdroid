import 'package:flutter/material.dart';

class ZPCButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final Color? bgColor;
  final Color? textColor;
  final String title;
  final bool isLoading;

  const ZPCButton(
      {Key? key,
      required this.onPressed,
      this.bgColor,
      this.textColor,
      required this.title,
      this.isLoading = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: 40,
      minWidth: 80,
      onPressed: onPressed,
      disabledColor: Colors.grey[800],
      disabledTextColor: Colors.grey[600],
      color: bgColor ?? Theme.of(context).primaryColorDark,
      textColor: textColor ?? Colors.white,
      child: isLoading == false
          ? Text(
              title.toUpperCase(),
              style: TextStyle(fontSize: 12),
            )
          : const SizedBox(
              width: 20.0,
              height: 20.0,
              child: CircularProgressIndicator(
                color: Colors.white,
              ),
            ),
    );
  }
}
