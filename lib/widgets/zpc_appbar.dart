import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/constants.dart';
import '../controllers/controllers.dart';

class ZPCAppBar extends GetWidget<AuthController> with PreferredSizeWidget {
  final bool showLeading;
  final bool showLogout;
  final bool showSearch;
  final String? title;
  final VoidCallback? searchCallback;

  const ZPCAppBar({
    Key? key,
    this.showLeading = true,
    this.showLogout = false,
    this.title,
    this.showSearch = false,
    this.searchCallback,
  }) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(50.0);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title ?? 'ZPCDroid'),
      backgroundColor: kMainColor,
      automaticallyImplyLeading: true,
      actions: <Widget>[
        showSearch
            ? Container(
                margin: const EdgeInsets.only(left: 20.0),
                child: IconButton(
                  icon: const Icon(Icons.search),
                  tooltip: 'Search',
                  onPressed: searchCallback,
                ))
            : Container(),
        cartCounter(context),
        // will conditionally show later

        showLogout
            ? Container(
                margin: const EdgeInsets.only(left: 20.0),
                child: IconButton(
                  icon: const Icon(Icons.logout_outlined),
                  tooltip: 'Logout',
                  onPressed: () async {
                    await controller.signOut();
                  },
                ),
              )
            : Container(),
      ],
    );
  }

  Widget cartCounter(BuildContext context) {
    final WishlistController controller = Get.find<WishlistController>();

    return Obx(
      () => Container(
        padding: EdgeInsets.all(5.0),
        child: Stack(
          children: [
            IconButton(
              iconSize: 24,
              icon: const Icon(Icons.favorite),
              tooltip: 'Show Cart',
              onPressed: () {
                Get.toNamed('/build-cart');
              },
            ),
            if (controller.itemCount > 0)
              Positioned(
                top: 10,
                right: 8,
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: Colors.red,
                  ),
                  width: 16,
                  height: 16,
                  child: Text(
                    controller.itemCount.toString(),
                    style: const TextStyle(fontSize: 9),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}
