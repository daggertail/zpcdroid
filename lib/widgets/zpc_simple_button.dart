import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ZPCSimpleButton extends StatelessWidget {
  final String title;
  VoidCallback onPressed;
  bool? isDisabled;

  ZPCSimpleButton(
      {Key? key,
      required this.title,
      required this.onPressed,
      this.isDisabled = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        decoration: BoxDecoration(
          color: isDisabled == true ? Colors.grey[700] : Colors.deepOrange,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Text(
          title,
          style: TextStyle(
            color: isDisabled == true ? Colors.grey : Colors.white,
          ),
        ),
      ),
    );
  }
}
