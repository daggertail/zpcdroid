import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/model/models.dart';
import 'package:zpcdroid/screens/screens.dart';
import 'package:zpcdroid/widgets/widgets.dart';

class BuildView extends StatelessWidget {
  final dynamic loadedBuildParts;
  final String title;
  final VoidCallback onEditTitlePressed;

  const BuildView({
    Key? key,
    required this.loadedBuildParts,
    required this.title,
    required this.onEditTitlePressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Category> categoryList = [
      Category(name: 'CPU', type: 'CPU'),
      Category(name: 'Motherboard', type: 'Motherboard'),
      Category(name: 'RAM', type: 'RAM'),
      Category(name: 'GPU', type: 'GPU'),
      Category(name: 'Storage', type: 'Storage'),
      Category(name: 'PSU', type: 'PSU'),
      Category(name: 'CPU Cooling', type: 'CPU Cooling'),
      Category(name: 'Casing', type: 'Casing'),
      Category(name: 'Fans', type: 'Fans')
    ];

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                title,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(width: 10),
              GestureDetector(
                onTap: () {
                  // Get.offNamed('/edit-build', parameters: {b : });
                },
                child: const Icon(
                  Icons.edit,
                  color: Colors.black45,
                ),
              )
            ],
          ),
          const Divider(),
          const SizedBox(height: 5.0),
          ...categoryList.map((category) {
            final part = loadedBuildParts[category.type];

            return Container(
              margin: const EdgeInsets.only(top: 20, bottom: 20),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        category.name,
                        style: Theme.of(context).textTheme.headline2,
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  if (part != null)
                    ProductCard(
                      onPressed: () {
                        Get.toNamed('/part-detail',
                            arguments: PartDetailScreenArgs(
                              part: part,
                              showActionButton: false,
                            ));
                      },
                      part: part,
                    )
                ],
              ),
            );
          }).toList()
        ],
      ),
    );
  }
}
