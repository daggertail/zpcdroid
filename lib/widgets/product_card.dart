import 'package:flutter/material.dart';
import 'package:zpcdroid/model/part_model.dart';
import '../functions.dart';

class ProductCard extends StatelessWidget {
  final Part part;
  final VoidCallback? onAddButtonPressed;
  final VoidCallback? onDeleteButtonPressed;
  final VoidCallback? onPressed;
  final VoidCallback? onEditButtonPressed;

  const ProductCard({
    Key? key,
    this.onAddButtonPressed,
    this.onDeleteButtonPressed,
    this.onEditButtonPressed,
    required this.onPressed,
    required this.part,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 3.0,
              blurRadius: 5.0,
            ),
          ],
          color: Colors.white,
        ),
        child: Row(children: [
          Container(
            margin: const EdgeInsets.only(right: 15.0),
            child: part.images.isNotEmpty
                ? Image.network(part.images[0], width: 100.0)
                : Image.network('https://dummyimage.com/600x600/2aa5bd/fff',
                    width: 100.0),
          ),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  part.name,
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.left,
                ),
                const SizedBox(height: 5),
                SpecText(part: part),
                const SizedBox(height: 15.0),
                Text(
                  'P ${formatPrice(part.price)}',
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                      fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          if (onAddButtonPressed != null)
            _buildAddButton()
          else if (onDeleteButtonPressed != null)
            _buildDeleteButton()
          else if (onEditButtonPressed != null)
            _buildEditButton()
        ]),
      ),
    );
  }

  Widget _buildEditButton() {
    return GestureDetector(
      onTap: onEditButtonPressed,
      child: Container(
        width: 20,
        height: 20,
        decoration: BoxDecoration(
          color: Colors.teal,
          borderRadius: BorderRadius.circular(50),
        ),
        alignment: Alignment.center,
        child: const Icon(
          Icons.edit,
          color: Colors.white,
          size: 10,
        ),
      ),
    );
  }

  Widget _buildAddButton() {
    return GestureDetector(
      onTap: onAddButtonPressed,
      child: Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
          color: Colors.teal,
          borderRadius: BorderRadius.circular(50),
        ),
        alignment: Alignment.center,
        child: const Icon(
          Icons.add,
          color: Colors.white,
          size: 16,
        ),
      ),
    );
  }

  Widget _buildDeleteButton() {
    return GestureDetector(
      onTap: onDeleteButtonPressed,
      child: Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(50),
        ),
        alignment: Alignment.center,
        child: const Icon(
          Icons.remove,
          color: Colors.white,
          size: 16,
        ),
      ),
    );
  }
}

class SpecText extends StatelessWidget {
  final Part part;
  const SpecText({Key? key, required this.part}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (part.category) {
      case 'CPU':
      case 'Motherboard':
        return Text("Socket: ${part.spec!['socket']}");
      case 'RAM':
        return Text("${part.spec!['mem_type']}");
      // case 'HDD':
      //   return Text(
      //       "Type: ${part.spec['storage_type']}   Capacity: ${part.spec['capacity']}");
      // case 'GPU':
      //   return Text("Memory: ${part.spec['memory']}");
      // case 'PSU':
      //   return Text("Rated: ${part.spec['rated_power']}");
      // case 'Casing':
      //   return Text("Support: ${part.spec['mobo_support']}");
    }

    return Container();
  }
}
