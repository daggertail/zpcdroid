import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/controllers/controllers.dart';
import '../widgets/widgets.dart';
import '../constants.dart';

class BottomNavBuild extends GetWidget<ManualBuildController> {
  final bool? showSaveButton;
  final double totalPrice;

  const BottomNavBuild({
    Key? key,
    this.showSaveButton = false,
    required this.totalPrice,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formatCurrency = Constants.numberFormat;

    return BottomAppBar(
      color: Colors.black,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 35),
        height: 70,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (showSaveButton == true)
              Obx(
                () => ZPCSimpleButton(
                    onPressed: () {
                      controller.showUpdateTitleDialog();
                    },
                    isDisabled: controller.isBuildEmpty,
                    title: 'Save this Build'),
              ),
            Row(
              children: [
                Text(
                  'TOTAL',
                  style: Theme.of(context)
                      .textTheme
                      .headline4!
                      .copyWith(color: Colors.white),
                ),
                SizedBox(width: 10),
                Text(
                  'P ${formatCurrency.format(totalPrice)}',
                  style: Theme.of(context)
                      .textTheme
                      .headline3!
                      .copyWith(color: Colors.white),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
