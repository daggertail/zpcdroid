import 'package:flutter/material.dart';
import 'package:zpcdroid/controllers/controllers.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/model/models.dart';

class SelectCategoryBar extends GetWidget<PartController> {
  const SelectCategoryBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Category> categoryList = [
      Category(name: 'All', type: 'All'),
      ...Category.categories
    ];

    return Container(
      width: double.infinity,
      color: const Color(0xFF13293D),
      // decoration: BoxDecoration(color: Colors.white),
      padding: const EdgeInsets.all(10.0),
      child: SizedBox(
        height: 50,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: categoryList.length,
          itemBuilder: (context, index) => Obx(
            () => CategoryButton(
                title: categoryList[index].name,
                isSelected:
                    controller.selectedCategory == categoryList[index].type
                        ? true
                        : false,
                onPressed: () {
                  controller.changeCategory(categoryList[index].type);
                }),
          ),
        ),
      ),
    );
  }
}

class CategoryButton extends StatelessWidget {
  final String title;
  final VoidCallback? onPressed;
  final bool isSelected;

  const CategoryButton({
    Key? key,
    this.title = '',
    this.onPressed,
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool _isSelected = isSelected;

    return isSelected
        ? ElevatedButton(
            child: Text(title, style: const TextStyle(color: Colors.white)),
            style: ElevatedButton.styleFrom(
              onSurface: Colors.white,
              textStyle: const TextStyle(fontSize: 14.0),
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 0),
            ),
            onPressed: _isSelected ? null : onPressed,
          )
        : TextButton(
            child: Text(
              title,
              style: const TextStyle(color: Colors.white),
            ),
            style: TextButton.styleFrom(
              textStyle: const TextStyle(fontSize: 14.0),
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 0),
            ),
            onPressed: onPressed,
          );
  }
}
