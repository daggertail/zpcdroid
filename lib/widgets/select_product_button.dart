import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/screens/screens.dart';
import '../widgets/widgets.dart';

class SelectProductButton extends StatelessWidget {
  final String title;
  final String category;
  final dynamic controller;

  const SelectProductButton({
    Key? key,
    required this.title,
    required this.category,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (controller.isPartAlreadyExistsWithinBuild(category) == true) {
        return Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: Column(
            children: [
              Text(category, style: Theme.of(context).textTheme.headline2),
              const SizedBox(height: 10),
              ProductCard(
                part: controller.build[category]!,
                onDeleteButtonPressed: () {
                  controller.removePart(controller.build[category]!);
                },
                onPressed: () {
                  Get.toNamed('/part-detail',
                      arguments: PartDetailScreenArgs(
                        part: controller.build[category]!,
                        showActionButton: false,
                      ));
                },
              )
            ],
          ),
        );
      } else {
        return _buildButton(context);
      }
    });
  }

  _buildButton(context) {
    return InkWell(
      onTap: () => _showbtsheet(context, category),
      child: Container(
        margin: const EdgeInsets.all(10.0),
        child: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          decoration: BoxDecoration(
              border: Border.all(
                color: Colors.lightBlueAccent,
                width: 1.0,
              ),
              color: Colors.indigo.shade900),
          child: Column(children: [
            Text(
              title,
              style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            const SizedBox(height: 10),
            Text(
              'Choose your $category.',
              style: const TextStyle(
                color: Colors.white,
                fontSize: 12.0,
              ),
            ),
          ]),
        ),
      ),
    );
  }

  _showbtsheet(context, category) async {
    var parts = await controller.getAllPartsByCategory(category);

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: const Color(0xFF0b0244),
      builder: (context) => SizedBox(
        height: MediaQuery.of(context).size.height,
        child: parts.isNotEmpty
            ? ListView.builder(
                itemCount: parts.length,
                padding: const EdgeInsets.symmetric(
                    vertical: 40.0, horizontal: 20.0),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ProductCard(
                      part: parts[index],
                      onAddButtonPressed: () {
                        Navigator.pop(context);
                        controller.addPart(parts[index]);
                      },
                      onPressed: () {
                        Navigator.pushNamed(context, '/part-detail',
                            arguments: PartDetailScreenArgs(
                              part: parts[index],
                              showActionButton: false,
                            ));
                      },
                    ),
                  );
                },
              )
            : Column(
                children: [
                  Expanded(
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'No parts showing yet. Maybe you need to select a motherboard or cpu component first.',
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .headline4!
                                .copyWith(color: Colors.white),
                          ),
                          const SizedBox(height: 40.0),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.transparent,
                                border: Border.all(color: Colors.white),
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(100))),
                            child: IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: const Icon(Icons.close),
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
