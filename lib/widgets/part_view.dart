import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:full_screen_image_null_safe/full_screen_image_null_safe.dart';
import 'package:zpcdroid/model/models.dart';
import 'package:photo_view/photo_view.dart';

import 'package:url_launcher/url_launcher.dart';

class PartView extends StatelessWidget {
  final Part part;

  const PartView({
    Key? key,
    required this.part,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            CarouselSlider(
              options: CarouselOptions(
                height: 200.0,
                autoPlay: true,
                autoPlayInterval: const Duration(seconds: 5),
                enlargeCenterPage: true,
                viewportFraction: 0.9,
                aspectRatio: 2.5,
                initialPage: 0,
              ),
              items: part.images.map((i) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: const BoxDecoration(color: Colors.white),
                        margin: const EdgeInsets.symmetric(
                            horizontal: 5.0, vertical: 10.0),
                        child: FullScreenWidget(
                          child: Center(
                            child: PhotoView(
                              backgroundDecoration:
                                  const BoxDecoration(color: Colors.white),
                              imageProvider: NetworkImage(i),
                            ),
                          ),
                        ));
                  },
                );
              }).toList(),
            ),
            const SizedBox(height: 20.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Chip(
                    label: Text(part.category,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2
                            ?.copyWith(color: Colors.white)),
                    backgroundColor: Colors.deepPurple,
                  ),
                  Text(
                    part.name,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline3,
                  ),
                  const SizedBox(height: 10.0),
                  Text(
                    'P ${double.parse(part.price).toStringAsFixed(2)}',
                    style: Theme.of(context).textTheme.headline4,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 30.0),
                  Text(
                    part.desc,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  const SizedBox(height: 30.0),
                  Text(
                    'Price last updated on: ${Part.convertTimeStamp(part.updatedOn)}',
                    textAlign: TextAlign.left,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(fontSize: 12, fontStyle: FontStyle.italic),
                  ),
                  const SizedBox(height: 20),
                  Container(
                      margin: const EdgeInsets.symmetric(
                          vertical: 30, horizontal: 0),
                      child: Column(
                        children: [
                          Text(
                            'COMPETITOR\'S PRICE',
                            style: Theme.of(context)
                                .textTheme
                                .headline4!
                                .copyWith(fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: 20),
                          if (part.competitorsPrice!.isNotEmpty)
                            ...part.competitorsPrice!
                                .map((c) => Container(
                                      margin: const EdgeInsets.symmetric(
                                          vertical: 10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              _launchURL(c['link']);
                                            },
                                            child: Text(
                                              c['storeName']!,
                                              style: const TextStyle(
                                                fontSize: 14,
                                                color: Colors.blue,
                                                fontStyle: FontStyle.italic,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            'P ${double.parse(c['price']!).toStringAsFixed(2)}',
                                            textAlign: TextAlign.left,
                                            style: const TextStyle(
                                              fontSize: 14,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                                .toList()
                          else
                            const Text('No data for competitors price yet.')
                        ],
                      ))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _launchURL(_url) async {
    if (!await launch(_url)) throw 'Could not launch $_url';
  }
}
