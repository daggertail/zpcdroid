import 'package:cloud_firestore/cloud_firestore.dart';

class Dictionary {
  final String title;
  final String description;
  String? citations;

  Dictionary({
    required this.title,
    required this.description,
    this.citations,
  });

  static Dictionary fromSnapshot(DocumentSnapshot snapshot) {
    Dictionary dictionary = Dictionary(
      title: snapshot['title'],
      description: snapshot['desc'],
      citations: snapshot['citations'] ?? '',
    );

    return dictionary;
  }
}
