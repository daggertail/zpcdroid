// ignore_for_file: non_constant_identifier_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'part_model.dart';
import 'timestamp_converter.dart';

part 'build_model.g.dart';

@JsonSerializable(explicitToJson: true, anyMap: true)
class Build {
  final String id;
  final String name;
  final Map<String, Part> parts;
  final String uid;
  final String price;
  final String status;

  @TimestampConverter()
  final DateTime created_on;

  @TimestampConverter()
  final DateTime updated_on;

  Build({
    required this.id,
    required this.name,
    required this.parts,
    required this.uid,
    required this.price,
    required this.created_on,
    required this.updated_on,
    required this.status,
  });

  factory Build.fromJson(Map<String, dynamic> data) => _$BuildFromJson(data);

  Map<String, dynamic> toJson() => _$BuildToJson(this);

  static convertTimeStamp(DateTime datetime) {
    String convertedDate;
    convertedDate = DateFormat('yyyy-MM-dd kk:mm').format(datetime);
    return convertedDate;
  }
}
