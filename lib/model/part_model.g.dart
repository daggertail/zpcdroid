// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'part_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Part _$PartFromJson(Map json) => Part(
      id: json['id'] as String,
      name: json['name'] as String,
      price: json['price'] as String,
      images: json['images'] as List<dynamic>,
      category: json['category'] as String,
      desc: json['desc'] as String,
      updatedOn:
          const TimestampConverter().fromJson(json['updatedOn'] as Timestamp),
      competitorsPrice: (json['competitorsPrice'] as List<dynamic>?)
          ?.map((e) => Map<String, String>.from(e as Map))
          .toList(),
      spec: (json['spec'] as Map?)?.map(
        (k, e) => MapEntry(k as String, e),
      ),
    );

Map<String, dynamic> _$PartToJson(Part instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'price': instance.price,
      'images': instance.images,
      'category': instance.category,
      'desc': instance.desc,
      'spec': instance.spec,
      'competitorsPrice': instance.competitorsPrice,
      'updatedOn': const TimestampConverter().toJson(instance.updatedOn),
    };
