import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:intl/intl.dart';
import 'timestamp_converter.dart';

part 'part_model.g.dart';

@JsonSerializable(anyMap: true)
class Part {
  final String id;
  final String name;
  final String price;
  final List<dynamic> images;
  final String category;
  final String desc;
  final Map<String, dynamic>? spec;
  final List<Map<String, String>>? competitorsPrice;

  @TimestampConverter()
  final DateTime updatedOn;

  Part({
    required this.id,
    required this.name,
    required this.price,
    required this.images,
    required this.category,
    required this.desc,
    required this.updatedOn,
    this.competitorsPrice,
    this.spec,
  });

  factory Part.fromJson(Map<String, dynamic> data) => _$PartFromJson(data);

  Map<String, dynamic> toJson() => _$PartToJson(this);

  static convertTimeStamp(DateTime datetime) {
    String convertedDate;
    convertedDate = DateFormat.yMMMMd('en_US').add_jm().format(datetime);
    return convertedDate;
  }
}
