// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'build_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Build _$BuildFromJson(Map json) => Build(
      id: json['id'] as String,
      name: json['name'] as String,
      parts: (json['parts'] as Map).map(
        (k, e) => MapEntry(
            k as String, Part.fromJson(Map<String, dynamic>.from(e as Map))),
      ),
      uid: json['uid'] as String,
      price: json['price'] as String,
      created_on:
          const TimestampConverter().fromJson(json['created_on'] as Timestamp),
      updated_on:
          const TimestampConverter().fromJson(json['updated_on'] as Timestamp),
      status: json['status'] as String,
    );

Map<String, dynamic> _$BuildToJson(Build instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'parts': instance.parts.map((k, e) => MapEntry(k, e.toJson())),
      'uid': instance.uid,
      'price': instance.price,
      'status': instance.status,
      'created_on': const TimestampConverter().toJson(instance.created_on),
      'updated_on': const TimestampConverter().toJson(instance.updated_on),
    };
