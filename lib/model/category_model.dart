class Category {
  final String name;
  final String type;
  final double? allottedBudget;

  Category({
    required this.name,
    required this.type,
    this.allottedBudget,
  });

  // The list of all pc parts categories
  static List<Category> get categories {
    return [
      Category(name: 'CPU', type: 'CPU', allottedBudget: 0.2),
      Category(name: 'Motherboard', type: 'Motherboard', allottedBudget: 0.15),
      Category(name: 'RAM', type: 'RAM', allottedBudget: 0.1),
      Category(name: 'Storage', type: 'Storage', allottedBudget: 0.1),
      Category(name: 'GPU', type: 'GPU', allottedBudget: 0.15),
      Category(name: 'PSU', type: 'PSU', allottedBudget: 0.1),
      Category(name: 'Casing', type: 'Casing', allottedBudget: 0.1),
      Category(name: 'CPU Cooling', type: 'CPU Cooling', allottedBudget: 0.05),
      Category(name: 'Fans', type: 'Fans', allottedBudget: 0.05),
      Category(name: 'Monitor', type: 'Monitor', allottedBudget: 0.05),
      Category(name: 'Mouse', type: 'Mouse', allottedBudget: 0.03),
      Category(name: 'Keyboard', type: 'Keyboard', allottedBudget: 0.03),
      Category(
          name: 'Audio Device', type: 'Audio Device', allottedBudget: 0.03),
    ];
  }

  // Create a map  containing the category type and their budget
  static budget(double setBudget) {
    Map<String, double> b = {};

    for (var i = 0; i < categories.length; i++) {
      b[categories[i].type] = categories[i].allottedBudget!;
    }

    return b;
  }
}
