import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

import 'part_model.dart';
import 'timestamp_converter.dart';
import 'package:intl/intl.dart';

part 'sales_model.g.dart';

enum SalesStatus {
  processing,
  pending,
  fresh,
  deleted,
  fullfilled,
}

@JsonSerializable(explicitToJson: true, anyMap: true)
class Sales {
  final String id;
  final String title;
  final List<Part> items;
  final String uid;
  final String? customerName;
  final String status;

  @TimestampConverter()
  final DateTime createdOn;

  @TimestampConverter()
  final DateTime updatedOn;

  Sales(
      {required this.id,
      required this.title,
      required this.items,
      required this.uid,
      required this.status,
      this.customerName,
      required this.createdOn,
      required this.updatedOn});

  factory Sales.fromJson(Map<String, dynamic> data) => _$SalesFromJson(data);
  Map<String, dynamic> toJson() => _$SalesToJson(this);

  static convertTimeStamp(DateTime datetime) {
    String convertedDate;
    convertedDate = DateFormat('yyyy-MM-dd kk:mm').format(datetime);
    return convertedDate;
  }
}
