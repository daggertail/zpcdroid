import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

import 'package:get/get.dart';
import './config/config.dart';
import './config/app_router.dart';
import './screens/screens.dart';
import 'bindings/zpc_bindings.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(ZPCDroid());
}

// ignore: use_key_in_widget_constructors
class ZPCDroid extends StatelessWidget {
  @override
  build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: zpctheme(context),
      onGenerateRoute: AppRouter.onGenerateRoute,
      initialRoute: WrapperScreen.routeName,
      initialBinding: ZPCBindings(),
    );
  }
}
