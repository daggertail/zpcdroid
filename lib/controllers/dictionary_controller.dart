import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/controllers/utility_mixin.dart';
import '../model/models.dart';
import '../services/dictionary_service.dart';

class DictionaryController extends GetxController with UtilityMixin {
  final RxList<Dictionary> dict = <Dictionary>[].obs;
  final RxList<Dictionary> searchedWord = <Dictionary>[].obs;
  TextEditingController textController = TextEditingController();

  @override
  void onInit() {
    dict.bindStream(DictionaryService().getAll());
    searchedWord.value = dict;
    super.onInit();
  }

  search() {
    String keyword = textController.text.toLowerCase().trim();

    if (keyword.isNotEmpty) {
      RegExp exp = RegExp("\\b" + keyword + "\\b", caseSensitive: false);
      searchedWord.value =
          dict.where((e) => exp.hasMatch(e.title.toLowerCase())).toList();
    } else {
      searchedWord.value = dict;
    }
  }

  void updateDB() async {
    await DictionaryService().updateDB();
    showSnackbar('success', 'Successfully updated', 'DB successfully updated');
  }
}
