import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../model/models.dart';
import '../services/tsguide_service.dart';

class TSGuideController extends GetxController {
  final RxList<TSGuide> tsguide = <TSGuide>[].obs;
  final RxList<TSGuide> searchedWord = <TSGuide>[].obs;

  final isTitleOpen = [].obs;

  TextEditingController textController = TextEditingController();

  @override
  void onInit() {
    tsguide.bindStream(TSGuideService().getAll());
    searchedWord.value = tsguide;
    super.onInit();
  }

  search() {
    String keyword = textController.text.toLowerCase().trim();

    if (keyword.isNotEmpty == true) {
      RegExp exp = RegExp("\\b" + keyword + "\\b", caseSensitive: false);
      searchedWord.value =
          tsguide.where((e) => exp.hasMatch(e.title.toLowerCase())).toList();
    } else {
      searchedWord.value = tsguide;
    }
  }
}
