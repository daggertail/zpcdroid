import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:zpcdroid/constants.dart';

abstract class UtilityMixin {
  void showSnackbar(String type, String title, String message) {
    Get.snackbar(
      title,
      message,
      animationDuration: const Duration(milliseconds: 500),
      duration: const Duration(milliseconds: 1500),
      backgroundColor: type == 'success'
          ? Colors.green[600]
          : type == 'error'
              ? Colors.red
              : Colors.amber,
      dismissDirection: SnackDismissDirection.HORIZONTAL,
      isDismissible: true,
    );
  }

  void showModal({
    required String title,
    required List<Widget> content,
    String? confirmText,
    String? cancelText,
    VoidCallback? confirm,
    VoidCallback? cancel,
  }) {
    Get.defaultDialog(
      titlePadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      contentPadding: const EdgeInsets.all(20),
      title: title,
      titleStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      content: Column(
        children: content.toList(),
      ),
      radius: 0,
      confirm: MaterialButton(
        elevation: 0,
        color: kMainColor,
        onPressed: confirm,
        child: Text(
          confirmText ?? 'Save',
          style: const TextStyle(color: Colors.white),
        ),
      ),
      cancel: MaterialButton(
        color: Colors.white,
        onPressed: cancel ??
            () {
              Get.back();
            },
        child: Text(cancelText ?? 'Cancel'),
      ),
    );
  }
}
