import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/controllers/utility_mixin.dart';
import 'package:zpcdroid/model/models.dart';
import 'package:zpcdroid/services/part_service.dart';

class PartController extends GetxController with UtilityMixin {
  final RxList<Part> _parts = <Part>[].obs;
  final RxString _selectedCategory = 'All'.obs;
  final RxBool isLoading = false.obs;

  final RxString searchKey = ''.obs;
  final RxList<Part> searchedParts = <Part>[].obs;

  get selectedCategory => _selectedCategory;
  get parts => _parts;

  @override
  void onInit() {
    _parts.bindStream(PartService().getAllParts());
    super.onInit();
  }

  getPartsByCategory(String categoryName) {
    try {
      isLoading.value = true;
      _parts.bindStream(PartService().getAllPartsByCategory(categoryName));
    } finally {
      isLoading.value = false;
    }
  }

  getAllParts() {
    try {
      isLoading.value = true;
      _parts.bindStream(PartService().getAllParts());
    } finally {
      isLoading.value = false;
    }
  }

  void changeCategory(String name) {
    _selectedCategory.value = name;

    if (_selectedCategory.value == 'All') {
      getAllParts();
    } else {
      getPartsByCategory(name);
    }
  }

  void search() {
    String keyword = searchKey.toLowerCase().trim();

    if (keyword.isNotEmpty) {
      RegExp exp = RegExp("\\b" + keyword + "\\b", caseSensitive: false);
      searchedParts.value =
          parts.where((e) => exp.hasMatch(e.name.toLowerCase())).toList();
    } else {
      searchedParts.value = parts;
    }
  }

  void clearSearch() {
    searchedParts.value = [];
    searchKey.value = '';
  }

  void showSearchModal() {
    final TextEditingController searchController = TextEditingController();
    showModal(
        title: 'Search Part',
        content: [
          TextFormField(
            decoration: const InputDecoration(
              labelStyle: TextStyle(fontSize: 12),
              labelText: 'Search by the name of the parts.',
              border: OutlineInputBorder(),
            ),
            controller: searchController,
          )
        ],
        confirmText: 'Search',
        confirm: () {
          searchKey.value = searchController.text;
          search();
          Get.back();
        });
  }
}
