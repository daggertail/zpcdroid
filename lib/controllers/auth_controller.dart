import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/widgets/snackbar.dart';
import '../widgets/widgets.dart';

class AuthController extends GetxController {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Rxn<User> _firebaseUser = Rxn<User>();
  final CollectionReference _userRef =
      FirebaseFirestore.instance.collection('users');

  final RxBool isLoading = false.obs;

  User? get user => _firebaseUser.value;

  String? get userId => _firebaseUser.value?.uid;

  @override
  void onInit() {
    _firebaseUser.bindStream(_auth.authStateChanges());
  }

  Future<void> createUser(
    String firstName,
    String lastName,
    String email,
    String password,
  ) async {
    try {
      var userValue = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      await _userRef
          .add({'firstName': firstName, 'lastName': lastName, 'email': email});
      Get.offAllNamed('/login');
    } on FirebaseException catch (e) {
      showSnackbar(
        title: 'Error Creating an Account',
        message: 'Error: ${e.code}',
        isError: true,
      );
    }
  }

  Future<void> signInUser(String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      Get.offAllNamed('/parts');
    } on FirebaseException catch (e) {
      showSnackbar(
        title: 'Error logging in',
        message: 'Invalid email/password',
        isError: true,
      );
    }
  }

  Future<void> signInAnonymously() async {
    isLoading.value = true;
    try {
      await _auth.signInAnonymously();
      Get.offAllNamed('/dashboard');
    } on FirebaseException catch (e) {
      showSnackbar(
        title: 'Error Signing in',
        message: 'There\'s an error signing in.',
        isError: true,
      );
    } finally {
      isLoading.value = true;
    }
  }

  Future<void> signOut() async {
    try {
      await _auth.signOut();
      Get.offAllNamed('/');
    } catch (e) {}
  }
}
