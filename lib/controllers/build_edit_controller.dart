import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/controllers/product_build_controller.dart';
import 'package:zpcdroid/model/models.dart';
import 'package:zpcdroid/services/sales_service.dart';
import '../services/build_service.dart';

class BuildEditController extends ProductBuildController {
  final TextEditingController customerNameController = TextEditingController();

  void showSendToStoreModal(Build build) {
    showModal(
      title: 'Send to store?',
      content: [
        Text(
          'Are you sure you want to order this build: ${build.name}?',
          textAlign: TextAlign.center,
        )
      ],
      confirm: () {
        final List<Part> items =
            build.parts.entries.map((v) => v.value).toList();
        sendToStore(build.id, items, build.name);
      },
      confirmText: 'Send to Store',
    );
  }

  void sendToStore(String id, List<Part> items, String name) async {
    await SalesService().sendToStore(items: items, title: name);
    await BuildService().updateStatus(id: id, status: 'Sent');

    Get.offAllNamed('/user-builds');
    showSnackbar('success', 'Yow', 'Items successfully sent for purchase.');
  }

  void showDeleteModal(Build build) {
    showModal(
      title: 'Delete this build?',
      content: [
        Text(
          'Are you sure you want to delete this build: ${build.name}?',
          textAlign: TextAlign.center,
        )
      ],
      confirm: () {
        deleteBuild(build.id);
      },
      confirmText: 'DELETE',
    );
  }

  void deleteBuild(id) async {
    isLoading.value = true;
    await BuildService().deleteBuild(id);
    Get.offNamed('/user-builds');

    showSnackbar('success', 'Yow', 'Build successfully deleted.');
    isLoading.value = false;

    build.value = {};
    title.value = '';
  }
}
