import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/services/sales_service.dart';

import '../services/build_service.dart';
import '../model/models.dart';
import 'utility_mixin.dart';

class UserBuildsController extends GetxController with UtilityMixin {
  final RxList<Build> build = <Build>[].obs;
  final RxBool isLoading = false.obs;
  final RxString buildTitle = ''.obs;
  final RxList<Part> parts = <Part>[].obs;

  @override
  void onInit() {
    build.bindStream(BuildService().getUserBuilds());
    super.onInit();
  }

  void deleteBuild(id) async {
    isLoading.value = true;
    await BuildService().deleteBuild(id);
    Get.offNamed('/user-build');

    showSnackbar('success', 'Yow', 'Build successfully deleted.');
    isLoading.value = false;
  }

  void showSendToStoreModal(Build build) {
    showModal(
      title: 'Send to store?',
      content: [
        Text(
          'Are you sure you want to order this build: ${build.name}?',
          textAlign: TextAlign.center,
        )
      ],
      confirm: () {
        final List<Part> items =
            build.parts.entries.map((v) => v.value).toList();
        sendToStore(build.id, items, build.name);
      },
      confirmText: 'Send to Store',
    );
  }

  void sendToStore(String id, List<Part> items, String name) async {
    await SalesService().sendToStore(items: items, title: name);
    await BuildService().updateStatus(id: id, status: 'Sent');

    Get.back();

    showSnackbar('success', 'Yow', 'Items successfully sent for purchase.');
  }

  void setTitle(String title) {
    buildTitle.value = title;
  }

  void showUpdateTitleDialog(String id, String name) {
    final TextEditingController buildNameTextController =
        TextEditingController();

    buildNameTextController.text = name;

    Get.defaultDialog(
      titlePadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      contentPadding: const EdgeInsets.all(20),
      title: "Title of Build",
      titleStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      content: Container(
        child: Column(
          children: [
            TextFormField(
              decoration: const InputDecoration(
                labelText: 'Build name',
                border: OutlineInputBorder(),
              ),
              controller: buildNameTextController,
            )
          ],
        ),
      ),
      radius: 0,
      confirm: MaterialButton(
        elevation: 0,
        color: Colors.deepOrange,
        onPressed: () {
          name = buildNameTextController.text;
          saveTitle(id, name);
          Get.back();
        },
        child: const Text('Save', style: TextStyle(color: Colors.white)),
      ),
      cancel: MaterialButton(
        color: Colors.white,
        onPressed: () {
          Get.back();
        },
        child: const Text('Cancel'),
      ),
    );
  }

  void saveTitle(String id, String newName) async {
    await BuildService().saveTitle(id, newName);
    buildTitle.value = newName;
    showSnackbar('success', 'Yow', 'Build name successfully updated.');
  }
}
