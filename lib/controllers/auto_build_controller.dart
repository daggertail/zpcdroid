import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zpcdroid/controllers/product_build_controller.dart';
import 'package:zpcdroid/model/models.dart';
import 'package:zpcdroid/widgets/widgets.dart';

import 'dart:math';

class AutoBuildController extends ProductBuildController {
  // the selected budget of the form
  final RxInt budget = 40000.obs;

  // set the budget
  void setBudget(p) {
    budget.value = p;
  }

  final TextEditingController budgetTextController = TextEditingController();

  // return the highest price part that is close to the set allocated budget
  Part getTheHighestPricePart(List<Part> parts) {
    // get the highest price but less than of the allotted budget
    return parts.reduce(
        (a, b) => double.parse(a.price) > double.parse(b.price) ? a : b);
  }

  // return the lowest price part
  Part getTheLowestPricePart(List<Part> parts) {
    return parts.reduce(
        (a, b) => double.parse(a.price) < double.parse(b.price) ? a : b);
  }

  // automatically prefilled the build object of the part that correspond to the allocated budget
  autoBuildPC() {
    if (build.isNotEmpty) {
      build.value = {};
    }

    // Allocated percentage of budget.
    final Map<String, double> allotedBudget =
        Category.budget(budget.value.toDouble());

    for (var key in allotedBudget.keys) {
      List<Part> filteredParts = parts
          .where((p) =>
              p.category == key &&
              double.parse(p.price) <= (allotedBudget[key]! * budget.value))
          .toList();

      // Check whether if there's a part within the allocated budget
      if (filteredParts.isNotEmpty) {
        switch (key) {
          case 'CPU':

            // remove the parts that are not met with the condition
            filteredParts.removeWhere((element) {
              return element.spec!.containsKey('arch')
                  ? element.spec!['arch'] != arch.value
                  : true;
            });

            if (budget.value < 40000) {
              // Get the highest price of the list
              build['CPU'] = getTheLowestPricePart(filteredParts);
            } else {
              build['CPU'] = getTheHighestPricePart(filteredParts);
            }

            // add to the list of ids collection
            break;

          case 'Motherboard':

            // remove the parts that are not met with the condition
            filteredParts.removeWhere((element) {
              return element.spec!.containsKey('socket')
                  ? element.spec!['socket'] != build['CPU']!.spec!['socket']
                  : true;
            });

            if (budget.value < 40000) {
              // Get the highest price of the list
              build['Motherboard'] = getTheLowestPricePart(filteredParts);
            } else {
              build['Motherboard'] = getTheHighestPricePart(filteredParts);
            }

            // add to the list of ids collection
            break;

          case 'RAM':

            // remove the parts that are not met with the condition
            filteredParts.removeWhere((element) {
              return element.spec!.containsKey('mem_type')
                  ? element.spec!['mem_type'] !=
                      build['Motherboard']!.spec!['mem_type']
                  : true;
            });

            if (budget.value < 40000) {
              // Get the highest price of the list
              build['RAM'] = getTheLowestPricePart(filteredParts);
            } else {
              build['RAM'] = getTheHighestPricePart(filteredParts);
            }

            // add to the list of ids collection
            break;

          default:
            if (budget.value < 40000) {
              // Get the highest price of the list
              build[key] = getTheLowestPricePart(filteredParts);
            } else {
              build[key] = getTheHighestPricePart(filteredParts);
            }
        }
      } else {
        // show a dialog
        Get.defaultDialog(
          title: 'We cannot build a PC with that budget yet.',
          titleStyle: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
          radius: 0,
          confirm: ZPCButton(
              onPressed: () {
                Get.back();
              },
              title: 'Got it!'),
          content: const Text(
            'We\'re suggesting that you increase your budget.',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16),
          ),
        );
        return;
      }
    }

    // For debug: do not remove.
    // for (var i = 0; i < idsOfPartsInsideBuild.length; i++) {
    //   // print('${listOfBudgetParts[i].category} : ${listOfBudgetParts[i].name}');
    //   print(idsOfPartsInsideBuild[i]);
    // }

    // for (var k in autoBuild.entries) {
    //   print('$k : ${autoBuild[k]}');
    // }

    Get.offNamed('/auto-build-detail');
  }
}
