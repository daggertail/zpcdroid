import 'package:get/get.dart';
import 'utility_mixin.dart';
import '../model/models.dart';
import '../services/build_service.dart';
import '../services/part_service.dart';

import 'package:flutter/material.dart';

abstract class ProductBuildController extends GetxController with UtilityMixin {
  // the selected arch of the form
  final RxString arch = 'intel'.obs;

  // the title of the build
  RxString title = 'My Build'.obs;

  // List of Part
  final RxList<Part> parts = <Part>[].obs;

  // The build object
  RxMap<String, Part> build = <String, Part>{}.obs;

  // the property to check if build has a complete parts
  final RxBool isBuildComplete = true.obs;

  // The flag for loading
  final RxBool isLoading = false.obs;

  // Temporary storage for selected specs
  final RxString selectedSocket = ''.obs;
  final RxString selectedMemType = ''.obs;

  // on initialize, load all parts in _part property
  @override
  void onInit() {
    parts.bindStream(PartService().getAllParts());
    super.onInit();
  }

  // set the architecture
  changeArch(String archName) {
    if (archName != arch.value) {
      arch.value = archName;
      build.value = {};
    }
  }

  // Check whether the selected part is already in build
  bool isPartAlreadyExistsWithinBuild(String category) {
    return build.containsKey(category);
  }

  // Get parts from firestore service by its category
  Future<List<Part>> getAllPartsByCategory(String category) async {
    try {
      isLoading.value = true;

      if (category == 'CPU') {
        return await PartService().getCPUByArch(arch.value);
      } else if (category == 'Motherboard') {
        return await PartService().getMoboBySocket(selectedSocket.value);
      } else if (category == 'RAM') {
        return await PartService().getMemoryByType(selectedMemType.value);
      } else {
        return await PartService().getAllPartsByCategoryName(category);
      }
    } catch (e) {
      throw 'Error: Cannot find $category parts';
    } finally {
      isLoading.value = false;
    }
  }

  // Show the form for changing the title of the build
  void showUpdateTitleDialog({bool isUpdate = false, String? id}) {
    final TextEditingController buildNameTextController =
        TextEditingController();

    buildNameTextController.text = title.value;

    showModal(
        title: 'Title of Build',
        content: [
          TextFormField(
            decoration: const InputDecoration(
              labelText: 'Build name',
              border: OutlineInputBorder(),
            ),
            controller: buildNameTextController,
          )
        ],
        confirmText: 'Save this Build',
        confirm: () {
          title.value = buildNameTextController.text;
          isUpdate ? updateBuild(id!) : saveBuild();
          Get.back();
        });
  }

  // Check if the build has complete parts
  // this is used in the saving button state
  void checkBuild() {
    for (var i = 0; i < Category.categories.length; i++) {
      if (build.containsKey(Category.categories[i].type) == false) {
        isBuildComplete.value = false;
        return;
      }
    }
    isBuildComplete.value = true;
  }

  // Save the build into database
  saveBuild() async {
    await BuildService().saveBuild(
        name: title.value, parts: build, price: totalPrice.toString());

    Get.offNamed('/user-builds');

    showSnackbar('success', 'Success', 'Build $title successfully saved.');

    // Reset the values after saved.
    build.value = {};
    title.value = 'My Build';
    ///////////////
  }

  // Save the build into database
  updateBuild(String id) async {
    await BuildService().updateBuild(
      id: id,
      name: title.value,
      parts: build,
      price: totalPrice.toString(),
    );
    Get.offNamed('/user-builds');

    showSnackbar('success', 'Success', 'Build $title successfully updated.');
  }

  // Check if the build is empty
  bool get isBuildEmpty {
    return build.isEmpty;
  }

  // Get the total price of the parts inside build
  double get totalPrice {
    return build.entries
        .map<double>((e) => double.parse(e.value.price))
        .fold(0.00, (double a, b) => a + b);
  }

  // add the selected part inside the build
  void addPart(Part part) {
    if (!build.containsKey(part.category)) {
      if (part.category == 'CPU') {
        build['CPU'] = part;
        selectedSocket.value = part.spec!['socket'];
      } else if (part.category == 'Motherboard') {
        build['Motherboard'] = part;
        selectedMemType.value = part.spec!['mem_type'];
      } else {
        build[part.category.toString()] = part;
      }
    }
    // check if the parts is already complete
    checkBuild();
  }

  // remove the selected part inside the build
  void removePart(Part part) {
    if (build.containsKey(part.category)) {
      if (part.category == 'CPU' ||
          (part.category == 'Motherboard' && build.containsKey('RAM'))) {
        build.remove('CPU');
        build.remove('RAM');
        build.remove('Motherboard');
      } else if (part.category == 'Motherboard') {
        build.remove('CPU');
        build.remove('Motherboard');
      } else {
        build.remove(part.category);
      }
    }
    // check if the parts is already complete
    checkBuild();
  }
}
