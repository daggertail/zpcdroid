import 'package:get/get.dart';
import 'package:zpcdroid/controllers/utility_mixin.dart';
import 'package:zpcdroid/services/part_service.dart';
import 'package:zpcdroid/services/sales_service.dart';
import '../model/models.dart';
import 'package:flutter/material.dart';

class WishlistController extends GetxController with UtilityMixin {
  RxInt itemCount = 0.obs;
  RxBool itemExists = false.obs;

  // The collection of parts selected for cart
  final RxList<Part> wishlist = <Part>[].obs;

  final TextEditingController customerNameController = TextEditingController();

  addItem(Part part) {
    if (!wishlist.any((p) => p.id == part.id)) {
      wishlist.add(part);
      itemCount.value++;
      showSnackbar(
          'success', 'Yeehaw!!', 'Part successfully added to wishlist.');
    } else {
      showSnackbar('error', 'Oh No!!', 'Part is already in the wishlist.');
    }
  }

  removeItem(Part part) {
    if (wishlist.any((p) => p.id == part.id)) {
      wishlist.removeWhere((p) => p.id == part.id);
      itemCount.value--;
      showSnackbar(
          'success', 'Yey!!', 'Part successfully removed from wishlist.');
    }
  }

  Future<void> updatePartDB() async {
    await PartService().updateDB();
  }

  void showSendToStoreModal() {
    showModal(
      title: 'Send to store?',
      content: [
        const Text(
          'Are you sure you want to order this parts?',
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 20),
        TextFormField(
          decoration: const InputDecoration(
            labelText: 'Customer name',
            border: OutlineInputBorder(),
          ),
          controller: customerNameController,
        )
      ],
      confirm: () {
        sendToStore(wishlist);
      },
      confirmText: 'Send to Store',
    );
  }

  void sendToStore(List<Part> items) async {
    await SalesService().sendPartsToStore(
        items: items, customerName: customerNameController.text);

    Get.offAllNamed('/build-cart');
    showSnackbar('success', 'Yow', 'Items successfully sent for purchase.');
  }

  isPartAlreadyExistsInCart(Part part) {
    if (wishlist.isNotEmpty) {
      return wishlist.any((p) => p.id == part.id);
    } else {
      return false;
    }
  }

  sendCartForPurchase() {
    if (wishlist.isNotEmpty) {
      Get.defaultDialog(
          content: const Text('This feature is not available yet.'));
    }
  }

  double get totalPrice {
    return wishlist
        .asMap()
        .entries
        .map<double>((e) => double.parse(e.value.price))
        .fold(0.00, (double a, b) => a + b);
  }
}
