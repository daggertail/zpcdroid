import 'package:get/get.dart';
import 'package:zpcdroid/controllers/tsguide_controller.dart';
import '../controllers/controllers.dart';

class ZPCBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AuthController>(() => AuthController());
    Get.lazyPut<PartController>(() => PartController());
    Get.lazyPut<ManualBuildController>(() => ManualBuildController());
    Get.lazyPut<UserBuildsController>(() => UserBuildsController());
    Get.lazyPut<AutoBuildController>(() => AutoBuildController());
    Get.lazyPut<BuildEditController>(() => BuildEditController());
    Get.lazyPut<DictionaryController>(() => DictionaryController());
    Get.lazyPut<TSGuideController>(() => TSGuideController());
    Get.lazyPut<WishlistController>(() => WishlistController());
  }
}
